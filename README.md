# END OF LIFE  
The Midnight for Minecraft 1.12.2 is no longer being updated. Due to this, combined with the fact that Dynamic Trees is not updating to 1.14.4 or 1.15.2, this mod is no longer being worked on. Feel free to browse the repository at your leisure, as it only exists for historical purposes.

# Dynamic Trees Mod Compatability for The Midnight  
Dynamic Trees Midnight Compat was an addon for Dynamic Trees to add support for The Midnight's trees.

## Where can you find us?  
If you want to discuss the mod, we have a [Discord server](https://discord.gg/Rdc86yA)!  
We have other mods we like to work on, too! You can view them by visiting our [GitHub page](https://github.com/Cryptic-Mushroom) and [Cipher_Zero_X's CurseForge page](https://www.curseforge.com/members/cipher_zero_x/projects).  
If you would like to support our work, consider donating to our [Patreon](https://www.patreon.com/crypticmushroom).

### Credits  
- **Programmers**: [Cat Core](https://github.com/arthurbambou)