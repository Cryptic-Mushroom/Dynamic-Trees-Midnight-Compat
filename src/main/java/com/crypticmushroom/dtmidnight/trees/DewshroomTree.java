package com.crypticmushroom.dtmidnight.trees;

import com.crypticmushroom.dtmidnight.DynamicTreesMidnight;
import com.crypticmushroom.dtmidnight.ModContent;
import com.ferreusveritas.dynamictrees.blocks.BlockBranch;
import com.ferreusveritas.dynamictrees.blocks.BlockDynamicSapling;
import com.ferreusveritas.dynamictrees.systems.GrowSignal;
import com.ferreusveritas.dynamictrees.trees.Species;
import com.ferreusveritas.dynamictrees.trees.TreeFamily;
import com.ferreusveritas.dynamictrees.util.SafeChunkBounds;
import com.mushroom.midnight.common.registry.ModBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.BiomeDictionary;

import java.util.Collections;
import java.util.List;

public class DewshroomTree extends TreeFamily {

    public class SpeciesDewshroom extends Species {
        SpeciesDewshroom(TreeFamily treeFamily) {
            super(treeFamily.getName(), treeFamily, ModContent.dewshroomLeavesProperty);

            setBasicGrowingParameters(0.15f, 12.0f, 0, 3, 0.9f);

            envFactor(BiomeDictionary.Type.SPOOKY, 1.0F);

            setDynamicSapling(new BlockDynamicSapling("dewshroomsapling").getDefaultState());

            generateSeed();
            setupStandardSeedDropping();

        }

        @Override
        public boolean isBiomePerfect(Biome biome) {
            return BiomeDictionary.hasType(biome, BiomeDictionary.Type.SPOOKY) && BiomeDictionary.hasType(biome, BiomeDictionary.Type.FOREST);
        }

//        @Override
//        protected int[] customDirectionManipulation(World world, BlockPos pos, int radius, GrowSignal signal, int probMap[]) {
//
//            EnumFacing originDir = signal.dir.getOpposite();
//
//            //Alter probability map for direction change
//            probMap[0] = 0;//Down is always disallowed for spruce
//            probMap[1] = signal.isInTrunk() ? getUpProbability(): 0;
//            probMap[2] = probMap[3] = probMap[4] = probMap[5] = //Only allow turns when we aren't in the trunk(or the branch is not a twig and step is odd)
//                    !signal.isInTrunk() || (signal.isInTrunk() && signal.numSteps % 2 == 1 && radius > 1) ? 2 : 0;
//            probMap[originDir.ordinal()] = 0;//Disable the direction we came from
//            probMap[signal.dir.ordinal()] += signal.isInTrunk() ? 0 : signal.numTurns == 1 ? 2 : 1;//Favor current travel direction
//
//            return probMap;
//        }
//
//        @Override
//        protected EnumFacing newDirectionSelected(EnumFacing newDir, GrowSignal signal) {
//            if(signal.isInTrunk() && newDir != EnumFacing.UP){//Turned out of trunk
//                signal.energy /= 3.0f;
//            }
//            return newDir;
//        }
//
//        @Override
//        public float getEnergy(World world, BlockPos pos) {
//            long day = world.getTotalWorldTime() / 24000L;
//            int month = (int)day / 30;//Change the hashs every in-game month
//
//            return super.getEnergy(world, pos) * biomeSuitability(world, pos) + (coordHashCode(pos.up(month)) % 5);//Vary the height energy by a psuedorandom hash function
//        }
//
//        public int coordHashCode(BlockPos pos) {
//            int hash = (pos.getX() * 9973 ^ pos.getY() * 8287 ^ pos.getZ() * 9721) >> 1;
//            return hash & 0xFFFF;
//        }
//
//        @Override
//        public void postGeneration(World world, BlockPos rootPos, Biome biome, int radius, List<BlockPos> endPoints, SafeChunkBounds safeBounds) {
//            //Manually place the highest few blocks of the conifer since the leafCluster voxmap won't handle it
//            BlockPos highest = Collections.max(endPoints, (a, b) -> a.getY() - b.getY());
//            world.setBlockState(highest.up(1), leavesProperties.getDynamicLeavesState(4));
//            world.setBlockState(highest.up(2), leavesProperties.getDynamicLeavesState(3));
//            world.setBlockState(highest.up(3), leavesProperties.getDynamicLeavesState(1));
//        }

        @Override
        public boolean isAcceptableSoilForWorldgen(World world, BlockPos pos, IBlockState soilBlockState) {
            return super.isAcceptableSoilForWorldgen(world, pos, soilBlockState) || soilBlockState.getBlock() == ModBlocks.MIDNIGHT_GRASS || soilBlockState.getBlock() == ModBlocks.MIDNIGHT_DIRT || soilBlockState.getBlock() == ModBlocks.NIGHTSTONE;
        }
    }

    public DewshroomTree() {
        super(new ResourceLocation(DynamicTreesMidnight.MODID, "dewshroom"));
        ModContent.dewshroomLeavesProperty.setTree(this);
    }

    @Override
    public void createSpecies() {
        setCommonSpecies(new SpeciesDewshroom(this));
    }

    @Override
    public int getRadiusForCellKit(IBlockAccess blockAccess, BlockPos pos, IBlockState blockState, EnumFacing dir, BlockBranch branch) {
        int radius = branch.getRadius(blockState);
        if(radius == 1) {
            if(blockAccess.getBlockState(pos.down()).getBlock() == branch) {
                return 128;
            }
        }
        return radius;
    }

    @Override
    public List<Block> getRegisterableBlocks(List<Block> blockList) {
        blockList.add(getCommonSpecies().getDynamicSapling().getBlock());
        return super.getRegisterableBlocks(blockList);
    }

    @Override
    public IBlockState getPrimitiveLog() {
        return ModBlocks.DEWSHROOM_STEM.getDefaultState();
    }

    @Override
    public ItemStack getPrimitiveLogItemStack(int qty) {
        ItemStack stack = new ItemStack(ModBlocks.DEWSHROOM_STEM);
        stack.grow(MathHelper.clamp(qty, 0, 64));
        return stack;
    }
}
