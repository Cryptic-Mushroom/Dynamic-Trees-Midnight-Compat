package com.crypticmushroom.dtmidnight.trees;

import com.crypticmushroom.dtmidnight.DynamicTreesMidnight;
import com.crypticmushroom.dtmidnight.ModContent;
import com.ferreusveritas.dynamictrees.ModTrees;
import com.ferreusveritas.dynamictrees.api.TreeRegistry;
import com.ferreusveritas.dynamictrees.blocks.BlockBranch;
import com.ferreusveritas.dynamictrees.blocks.BlockDynamicSapling;
import com.ferreusveritas.dynamictrees.systems.GrowSignal;
import com.ferreusveritas.dynamictrees.trees.Species;
import com.ferreusveritas.dynamictrees.trees.TreeFamily;
import com.ferreusveritas.dynamictrees.util.SafeChunkBounds;
import com.mushroom.midnight.common.registry.ModBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.BiomeDictionary;

import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;

public class ShadowrootTree extends TreeFamily {

    public class SpeciesShadowroot extends Species {
        SpeciesShadowroot(TreeFamily treeFamily) {
            super(treeFamily.getName(), treeFamily, ModContent.shadowrootLeavesProperty);

            setBasicGrowingParameters(0.30f, 18.0f, 4, 6, 0.8f);

            envFactor(BiomeDictionary.Type.SPOOKY, 1.0F);

            setDynamicSapling(new BlockDynamicSapling("shadowrootsapling").getDefaultState());

            generateSeed();
            setupStandardSeedDropping();
        }

        @Override
        public boolean isBiomePerfect(Biome biome) {
            return BiomeDictionary.hasType(biome, BiomeDictionary.Type.SPOOKY) && BiomeDictionary.hasType(biome, BiomeDictionary.Type.FOREST);
        }

        @Override
        public int[] customDirectionManipulation(World world, BlockPos pos, int radius, GrowSignal signal, int probMap[]) {
            probMap[EnumFacing.UP.getIndex()] = 4;

            //Disallow up/down turns after having turned out of the trunk once.
            if(!signal.isInTrunk()) {
                probMap[EnumFacing.UP.getIndex()] = 0;
                probMap[EnumFacing.DOWN.getIndex()] = 0;
                probMap[signal.dir.ordinal()] *= 0.35;//Promotes the zag of the horizontal branches
            }

            //Amplify cardinal directions to encourage spread the higher we get
            float energyRatio = signal.delta.getY() / this.getEnergy(world, pos);
            float spreadPush = energyRatio * 2;
            spreadPush = spreadPush < 1.0f ? 1.0f : spreadPush;
            for(EnumFacing dir: EnumFacing.HORIZONTALS) {
                probMap[dir.ordinal()] *= spreadPush;
            }

            //Ensure that the branch gets out of the trunk at least two blocks so it won't interfere with new side branches at the same level
            if(signal.numTurns == 1 && signal.delta.distanceSq(0, signal.delta.getY(), 0) == 1.0 ) {
                for(EnumFacing dir: EnumFacing.HORIZONTALS) {
                    if(signal.dir != dir) {
                        probMap[dir.ordinal()] = 0;
                    }
                }
            }

            //If the side branches are too swole then give some other branches a chance
            if(signal.isInTrunk()) {
                for(EnumFacing dir: EnumFacing.HORIZONTALS) {
                    if(probMap[dir.ordinal()] >= 7) {
                        probMap[dir.ordinal()] = 2;
                    }
                }
                if(signal.delta.getY() > this.getLowestBranchHeight() + 5) {
                    probMap[EnumFacing.UP.ordinal()] = 0;
                    signal.energy = 2;
                }
            }

            return probMap;	}

        @Override
        public EnumFacing newDirectionSelected(EnumFacing newDir, GrowSignal signal) {
            return newDir;
        }

        @Override
        public int getLowestBranchHeight(World world, BlockPos pos) {
            return (int)(super.getLowestBranchHeight(world, pos) * biomeSuitability(world, pos));
        }

        @Override
        public float getEnergy(World world, BlockPos pos) {
            return super.getEnergy(world, pos) * biomeSuitability(world, pos);
        }

        @Override
        public float getGrowthRate(World world, BlockPos pos) {
            return super.getGrowthRate(world, pos) * biomeSuitability(world, pos);
        }


        @Override
        public boolean isAcceptableSoilForWorldgen(World world, BlockPos pos, IBlockState soilBlockState) {
            return super.isAcceptableSoilForWorldgen(world, pos, soilBlockState)
                    || soilBlockState.getBlock() == ModBlocks.MIDNIGHT_GRASS
                    || soilBlockState.getBlock() == ModBlocks.MIDNIGHT_DIRT
                    || soilBlockState.getBlock() == ModBlocks.DECEITFUL_MUD
                    || soilBlockState.getBlock() == ModBlocks.DECEITFUL_PEAT;
        }

        protected BiFunction<Integer, Integer, Integer> getRootScaler() {
            return (inRadius, trunkRadius) -> {
                float scale = MathHelper.clamp(trunkRadius >= 13 ? (trunkRadius / 24f) : 0, 0, 1);
                return (int) (inRadius * scale);
            };
        }
    }

    public ShadowrootTree() {
        super(new ResourceLocation(DynamicTreesMidnight.MODID, "shadowroot"));
        ModContent.shadowrootLeavesProperty.setTree(this);
    }

    @Override
    public void createSpecies() {
        setCommonSpecies(new SpeciesShadowroot(this));
    }

    @Override
    public int getRadiusForCellKit(IBlockAccess blockAccess, BlockPos pos, IBlockState blockState, EnumFacing dir, BlockBranch branch) {
        int radius = branch.getRadius(blockState);
        if(radius == 1) {
            if(blockAccess.getBlockState(pos.down()).getBlock() == branch) {
                return 128;
            }
        }
        return radius;
    }

    @Override
    public List<Block> getRegisterableBlocks(List<Block> blockList) {
        blockList.add(getCommonSpecies().getDynamicSapling().getBlock());
        return super.getRegisterableBlocks(blockList);
    }

    @Override
    public IBlockState getPrimitiveLog() {
        return ModBlocks.SHADOWROOT_LOG.getDefaultState();
    }

    @Override
    public ItemStack getPrimitiveLogItemStack(int qty) {
        ItemStack stack = new ItemStack(ModBlocks.SHADOWROOT_LOG);
        stack.grow(MathHelper.clamp(qty, 0, 64));
        return stack;
    }
}
