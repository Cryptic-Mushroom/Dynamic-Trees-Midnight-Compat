package com.crypticmushroom.dtmidnight.worldgen;

import com.crypticmushroom.dtmidnight.DynamicTreesMidnight;
import com.ferreusveritas.dynamictrees.api.TreeRegistry;
import com.ferreusveritas.dynamictrees.api.worldgen.BiomePropertySelectors;
import com.ferreusveritas.dynamictrees.api.worldgen.IBiomeDataBasePopulator;
import com.ferreusveritas.dynamictrees.trees.Species;
import com.ferreusveritas.dynamictrees.worldgen.BiomeDataBase;
import com.mushroom.midnight.common.registry.ModBiomes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;

import java.util.ArrayList;
import java.util.Optional;

public class BiomeDataBasePopulator implements IBiomeDataBasePopulator {

    private static Species shadowroot, darkWillow;
    private static Species nightshroom, viridshroom,dewshroom;

    @Override
    public void populate(BiomeDataBase biomeDataBase) {
        shadowroot = TreeRegistry.findSpecies(new ResourceLocation(DynamicTreesMidnight.MODID, "shadowroot"));
        darkWillow = TreeRegistry.findSpecies(new ResourceLocation(DynamicTreesMidnight.MODID, "darkwillow"));

        addSpeciesSelector(biomeDataBase, ModBiomes.VIGILANT_FOREST, new BiomePropertySelectors.RandomSpeciesSelector().add(shadowroot, 2).add(darkWillow, 1));
        addDensitySelector(biomeDataBase, ModBiomes.VIGILANT_FOREST, scale(0.05F));

        addSpeciesSelector(biomeDataBase, ModBiomes.DECEITFUL_BOG, new BiomePropertySelectors.RandomSpeciesSelector().add(shadowroot, 1).add(darkWillow, 1));
        addDensitySelector(biomeDataBase, ModBiomes.DECEITFUL_BOG, scale(0.05F));

        addSpeciesSelector(biomeDataBase, ModBiomes.NIGHT_PLAINS, new BiomePropertySelectors.RandomSpeciesSelector().add(shadowroot, 1));
        addDensitySelector(biomeDataBase, ModBiomes.NIGHT_PLAINS, scale(0.001F));

//        ArrayList<Biome> blackList = new ArrayList<Biome>();
//        blackList.add(ModBiomes.BLACK_RIDGE);
//        blackList.add(ModBiomes.CRYSTAL_SPIRES);
//        blackList.add(ModBiomes.FUNGI_FOREST);
//        blackList.add(ModBiomes.OBSCURED_PEAKS);
//        blackList.add(ModBiomes.OBSCURED_PLATEAU);
//        blackList.add(ModBiomes.PHANTASMAL_VALLEY);
//        blackList.add(ModBiomes.WARPED_FIELDS);
    }

    private void addSpeciesSelector(BiomeDataBase dbase, Optional<Biome> biome, BiomePropertySelectors.ISpeciesSelector selector) {
        if(biome.isPresent()) {
            addSpeciesSelector(dbase, biome.get(), selector);
        }
    }

    private void addSpeciesSelector(BiomeDataBase dbase, Biome biome, BiomePropertySelectors.ISpeciesSelector selector) {
        dbase.setSpeciesSelector(biome, selector, BiomeDataBase.Operation.REPLACE);
    }

    private void addDensitySelector(BiomeDataBase dbase, Biome biome, BiomePropertySelectors.IDensitySelector selector) {
        dbase.setDensitySelector(biome, selector, BiomeDataBase.Operation.REPLACE);
    }

    private BiomePropertySelectors.IDensitySelector scale() {
        return (rnd, nd) -> nd;
    }

    private BiomePropertySelectors.IDensitySelector scale(double factor1) {
        return (rnd, nd) -> nd * factor1;
    }

    private BiomePropertySelectors.IDensitySelector scale(double factor1, double addend) {
        return (rnd, nd) -> (nd * factor1) + addend;
    }

    private BiomePropertySelectors.IDensitySelector scale(double factor1, double addend, double factor2) {
        return (rnd, nd) -> ((nd * factor1) + addend) * factor2;
    }
}
