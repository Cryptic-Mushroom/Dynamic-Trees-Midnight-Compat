package com.crypticmushroom.dtmidnight.proxy;

import com.crypticmushroom.dtmidnight.worldgen.BiomeDataBasePopulator;
import com.ferreusveritas.dynamictrees.api.WorldGenRegistry;

public class CommonProxy {

    public void preInit() {
    }

    public void init() {
        WorldGenRegistry.registerBiomeDataBasePopulator(new BiomeDataBasePopulator());
    }

    public void postInit() {
    }

}
