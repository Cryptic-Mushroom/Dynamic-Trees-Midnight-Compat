package com.crypticmushroom.dtmidnight;

import com.mushroom.midnight.Midnight;
import com.mushroom.midnight.common.biome.MidnightBiomeConfig;
import com.mushroom.midnight.common.event.BuildBiomeConfigEvent;
import com.mushroom.midnight.common.registry.ModBiomes;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class BiomeDecorationHandler {

    @SubscribeEvent
    public void BiomeDecorationHandlers(BuildBiomeConfigEvent event) {
        event.clear();
        if (event.getBiome().getUnlocalizedName().equals("biome.midnight.black_ridge.name")) {
            event.set(MidnightBiomeConfig.builder(BiomeConfigs.BLACK_RIDGE_CONFIG));
        } else if (event.getBiome().getUnlocalizedName().equals("biome.midnight.crystal_spires.name")) {
            event.set(MidnightBiomeConfig.builder(BiomeConfigs.CRYSTAL_SPIRES_CONFIG));
        }
        else if (event.getBiome().getUnlocalizedName().equals("biome.midnight.deceitful_bog.name")) {
            event.set(MidnightBiomeConfig.builder(BiomeConfigs.DECEITFUL_BOG_CONFIG));
        }
        else if (event.getBiome().getUnlocalizedName().equals("biome.midnight.fungi_forest.name")) {
            event.set(MidnightBiomeConfig.builder(BiomeConfigs.FUNGI_FOREST_CONFIG));
        }
        else if (event.getBiome().getUnlocalizedName().equals("biome.midnight.night_plains.name")) {
            event.set(MidnightBiomeConfig.builder(BiomeConfigs.NIGHT_PLAINS_CONFIG));
        }
        else if (event.getBiome().getUnlocalizedName().equals("biome.midnight.obscured_peaks.name")) {
            event.set(MidnightBiomeConfig.builder(BiomeConfigs.OBSCURED_PEAKS_CONFIG));
        }
        else if (event.getBiome().getUnlocalizedName().equals("biome.midnight.obscured_plateau.name")) {
            event.set(MidnightBiomeConfig.builder(BiomeConfigs.BLACK_PLATEAU_CONFIG));
        }
        else if (event.getBiome().getUnlocalizedName().equals("biome.midnight.phantasmal_valley.name")) {
            event.set(MidnightBiomeConfig.builder(BiomeConfigs.PHANTASMAL_VALLEY_CONFIG));
        }
        else if (event.getBiome().getUnlocalizedName().equals("biome.midnight.vigilant_forest.name")) {
            event.set(MidnightBiomeConfig.builder(BiomeConfigs.VIGILANT_FOREST_CONFIG));
        }
        else if (event.getBiome().getUnlocalizedName().equals("biome.midnight.warped_fields.name")) {
            event.set(MidnightBiomeConfig.builder(BiomeConfigs.WARPED_FIELDS_CONFIG));
        }
    }
}
