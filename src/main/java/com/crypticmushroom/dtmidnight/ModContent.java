package com.crypticmushroom.dtmidnight;

import com.crypticmushroom.dtmidnight.trees.*;
import com.ferreusveritas.dynamictrees.ModRecipes;
import com.ferreusveritas.dynamictrees.api.TreeHelper;
import com.ferreusveritas.dynamictrees.api.TreeRegistry;
import com.ferreusveritas.dynamictrees.api.client.ModelHelper;
import com.ferreusveritas.dynamictrees.api.treedata.ILeavesProperties;
import com.ferreusveritas.dynamictrees.blocks.LeavesProperties;
import com.ferreusveritas.dynamictrees.trees.Species;
import com.ferreusveritas.dynamictrees.trees.TreeFamily;
import com.mushroom.midnight.common.biome.MidnightBiomeConfig;
import com.mushroom.midnight.common.event.BuildBiomeConfigEvent;
import com.mushroom.midnight.common.registry.ModBiomes;
import com.mushroom.midnight.common.registry.ModBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IForgeRegistry;

import java.util.ArrayList;
import java.util.Collections;

@Mod.EventBusSubscriber(modid = DynamicTreesMidnight.MODID)
@GameRegistry.ObjectHolder(DynamicTreesMidnight.MODID)
public class ModContent {
    public static ILeavesProperties shadowrootLeavesProperty, darkWillowLeavesProperty, nightshroomLeavesProperty, viridshroomLeavesProperty, dewshroomLeavesProperty;
    public static ArrayList<TreeFamily> trees = new ArrayList<TreeFamily>();

    @SubscribeEvent
    public static void registerBlocks(final RegistryEvent.Register<Block> event) {
        IForgeRegistry<Block> registry = event.getRegistry();
        shadowrootLeavesProperty = new LeavesProperties(
                ModBlocks.SHADOWROOT_LEAVES.getDefaultState(),
                new ItemStack(ModBlocks.SHADOWROOT_LEAVES,1, 0),
                TreeRegistry.findCellKit("darkoak")){
            @Override
            public int getSmotherLeavesMax() {
                return 5;
            }
            @Override
            public int foliageColorMultiplier(IBlockState state, IBlockAccess world, BlockPos pos) {
                return 0x8F6DBC;
            }
        };

        darkWillowLeavesProperty = new LeavesProperties(
                ModBlocks.DARK_WILLOW_LEAVES.getDefaultState(),
                new ItemStack(ModBlocks.DARK_WILLOW_LEAVES,1, 0)){
            @Override
            public int getSmotherLeavesMax() {
                return 5;
            }
//            @Override
//            public int foliageColorMultiplier(IBlockState state, IBlockAccess world, BlockPos pos) {
//                return 0x8F6DBC;
//            }
        };

//        nightshroomLeavesProperty = new LeavesProperties(
//                ModBlocks.NIGHTSHROOM_HAT.getDefaultState(),
//                new ItemStack(ModBlocks.NIGHTSHROOM_HAT,1, 0),
//                TreeRegistry.findCellKit("conifer")){
//            @Override
//            public int getSmotherLeavesMax() {
//                return 5;
//            }
////            @Override
////            public int foliageColorMultiplier(IBlockState state, IBlockAccess world, BlockPos pos) {
////                return 0x8F6DBC;
////            }
//        };
//
//        viridshroomLeavesProperty = new LeavesProperties(
//                ModBlocks.VIRIDSHROOM_HAT.getDefaultState(),
//                new ItemStack(ModBlocks.VIRIDSHROOM_HAT,1, 0),
//                TreeRegistry.findCellKit("conifer")){
//            @Override
//            public int getSmotherLeavesMax() {
//                return 5;
//            }
////            @Override
////            public int foliageColorMultiplier(IBlockState state, IBlockAccess world, BlockPos pos) {
////                return 0x8F6DBC;
////            }
//        };
//
//        dewshroomLeavesProperty = new LeavesProperties(
//                ModBlocks.DEWSHROOM_HAT.getDefaultState(),
//                new ItemStack(ModBlocks.DEWSHROOM_HAT,1, 0),
//                TreeRegistry.findCellKit("conifer")){
//            @Override
//            public int getSmotherLeavesMax() {
//                return 5;
//            }
////            @Override
////            public int foliageColorMultiplier(IBlockState state, IBlockAccess world, BlockPos pos) {
////                return 0x8F6DBC;
////            }
//        };

        TreeHelper.getLeavesBlockForSequence(DynamicTreesMidnight.MODID, 0, shadowrootLeavesProperty);
        TreeFamily shadowrootTree = new ShadowrootTree();
        shadowrootTree.getCommonSpecies().addAcceptableSoil(ModBlocks.MIDNIGHT_GRASS, ModBlocks.MIDNIGHT_DIRT);

        TreeHelper.getLeavesBlockForSequence(DynamicTreesMidnight.MODID, 4, darkWillowLeavesProperty);
        TreeFamily darkWillow = new DarkWillowTree();
        darkWillow.getCommonSpecies().addAcceptableSoil(ModBlocks.MIDNIGHT_GRASS, ModBlocks.MIDNIGHT_DIRT);

//        TreeHelper.getLeavesBlockForSequence(DynamicTreesMidnight.MODID, 8, nightshroomLeavesProperty);
//        TreeFamily nightshroom = new NightshroomTree();
//        nightshroom.getCommonSpecies().addAcceptableSoil(ModBlocks.MIDNIGHT_GRASS, ModBlocks.MIDNIGHT_DIRT, ModBlocks.NIGHTSTONE);
//
//        TreeHelper.getLeavesBlockForSequence(DynamicTreesMidnight.MODID, 12, viridshroomLeavesProperty);
//        TreeFamily viridshroom = new ViridshroomTree();
//        viridshroom.getCommonSpecies().addAcceptableSoil(ModBlocks.MIDNIGHT_GRASS, ModBlocks.MIDNIGHT_DIRT, ModBlocks.NIGHTSTONE);
//
//        TreeHelper.getLeavesBlockForSequence(DynamicTreesMidnight.MODID, 16, dewshroomLeavesProperty);
//        TreeFamily dewshroom = new DewshroomTree();
//        dewshroom.getCommonSpecies().addAcceptableSoil(ModBlocks.MIDNIGHT_GRASS, ModBlocks.MIDNIGHT_DIRT, ModBlocks.NIGHTSTONE);

        Collections.addAll(trees, shadowrootTree);
        Collections.addAll(trees, darkWillow);
//        Collections.addAll(trees, nightshroom);
//        Collections.addAll(trees, viridshroom);
//        Collections.addAll(trees, dewshroom);
        trees.forEach(tree -> tree.registerSpecies(Species.REGISTRY));
        ArrayList<Block> treeBlocks = new ArrayList<>();
        trees.forEach(tree -> tree.getRegisterableBlocks(treeBlocks));
        treeBlocks.addAll(TreeHelper.getLeavesMapForModId(DynamicTreesMidnight.MODID).values());
        registry.registerAll(treeBlocks.toArray(new Block[treeBlocks.size()]));
    }

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event) {
        IForgeRegistry<Item> registry = event.getRegistry();

        TreeRegistry.findSpecies(new ResourceLocation(DynamicTreesMidnight.MODID, "shadowroot")).addAcceptableSoil(ModBlocks.MIDNIGHT_GRASS, ModBlocks.MIDNIGHT_DIRT);
        registry.registerAll(
                TreeRegistry.findSpecies(new ResourceLocation(DynamicTreesMidnight.MODID, "shadowroot")).getSeed()
        );

        TreeRegistry.findSpecies(new ResourceLocation(DynamicTreesMidnight.MODID, "darkwillow")).addAcceptableSoil(ModBlocks.MIDNIGHT_GRASS, ModBlocks.MIDNIGHT_DIRT);
        registry.registerAll(
                TreeRegistry.findSpecies(new ResourceLocation(DynamicTreesMidnight.MODID, "darkwillow")).getSeed()
        );

//        TreeRegistry.findSpecies(new ResourceLocation(DynamicTreesMidnight.MODID, "nightshroom")).addAcceptableSoil(ModBlocks.MIDNIGHT_GRASS, ModBlocks.MIDNIGHT_DIRT, ModBlocks.NIGHTSTONE);
//        registry.registerAll(
//                TreeRegistry.findSpecies(new ResourceLocation(DynamicTreesMidnight.MODID, "nightshroom")).getSeed()
//        );
//
//        TreeRegistry.findSpecies(new ResourceLocation(DynamicTreesMidnight.MODID, "viridshroom")).addAcceptableSoil(ModBlocks.MIDNIGHT_GRASS, ModBlocks.MIDNIGHT_DIRT, ModBlocks.NIGHTSTONE);
//        registry.registerAll(
//                TreeRegistry.findSpecies(new ResourceLocation(DynamicTreesMidnight.MODID, "viridshroom")).getSeed()
//        );
//        TreeRegistry.findSpecies(new ResourceLocation(DynamicTreesMidnight.MODID, "dewshroom")).addAcceptableSoil(ModBlocks.MIDNIGHT_GRASS, ModBlocks.MIDNIGHT_DIRT, ModBlocks.NIGHTSTONE);
//        registry.registerAll(
//                TreeRegistry.findSpecies(new ResourceLocation(DynamicTreesMidnight.MODID, "dewshroom")).getSeed()
//        );

        ArrayList<Item> treeItems = new ArrayList<>();
        trees.forEach(tree -> tree.getRegisterableItems(treeItems));
        registry.registerAll(treeItems.toArray(new Item[treeItems.size()]));
    }

    @SubscribeEvent
    public static void registerRecipes(RegistryEvent.Register<IRecipe> event) {
        setupRecipes("shadowroot", ModBlocks.SHADOWROOT_SAPLING);
        setupRecipes("darkwillow", ModBlocks.DARK_WILLOW_SAPLING);
//        setupRecipes("nightshroom", ModBlocks.NIGHTSHROOM);
//        setupRecipes("viridshroom", ModBlocks.VIRIDSHROOM);
//        setupRecipes("dewshroom", ModBlocks.DEWSHROOM);
    }

    public static void setupRecipes(String species, Block sapling) {
        Species specie = TreeRegistry.findSpecies(new ResourceLocation(DynamicTreesMidnight.MODID, species));
        ItemStack seed = specie.getSeedStack(1);
        ModRecipes.createDirtBucketExchangeRecipes(new ItemStack(sapling), seed, true);
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public static void registerModels(ModelRegistryEvent event) {
        for(TreeFamily tree : ModContent.trees) {
            ModelHelper.regModel(tree.getDynamicBranch());
            ModelHelper.regModel(tree.getCommonSpecies().getSeed());
            ModelHelper.regModel(tree);
        }
    }
}
