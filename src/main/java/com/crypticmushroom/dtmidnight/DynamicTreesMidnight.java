package com.crypticmushroom.dtmidnight;

import com.crypticmushroom.dtmidnight.proxy.CommonProxy;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(
        modid = DynamicTreesMidnight.MODID,
        name = DynamicTreesMidnight.NAME,
        version = DynamicTreesMidnight.VERSION,
        dependencies = DynamicTreesMidnight.DEPENDENCIES
)
public class DynamicTreesMidnight {
    public static final String MODID = "dtmidnight";
    public static final String NAME = "Dynamic Trees Midnight";
    public static final String VERSION = "@VERSION@";
    public static final String DEPENDENCIES = "required-after:dynamictrees@[1.12.2-0.8.2,);required-after:midnight";

    @Mod.Instance
    public static DynamicTreesMidnight instance;

    public static final Logger logger = LogManager.getLogger(MODID);

    @SidedProxy(clientSide = "com.crypticmushroom.dtmidnight.proxy.ClientProxy", serverSide = "com.crypticmushroom.dtmidnight.proxy.CommonProxy")
    public static CommonProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        proxy.preInit();
        MinecraftForge.EVENT_BUS.register(new BiomeDecorationHandler());
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init();
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        proxy.postInit();
    }
}
